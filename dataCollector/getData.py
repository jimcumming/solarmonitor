from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymongo import MongoClient
import datetime
 
client = ModbusClient(method = 'rtu', port = '/dev/ttyUSB0', baudrate = 115200)
client.connect()

# get battery and pv data
result = client.read_input_registers(0x3100,6,unit=1)

data = { 'pvVoltage' : float(result.registers[0] / 100.0), 'pvCurrent' :   float(result.registers[1] / 100.0), \
		'pvPowerL' : float(result.registers[2] / 100.0), \
		'batteryVoltage' : float(result.registers[4] / 100.0), 'chargeCurrent' : float(result.registers[5] / 100.0)}

# get load data

result = client.read_input_registers(0x310C,4,unit=1)

data['loadVoltage'] = float(result.registers[0] / 100.0)
data['loadCurrent'] = float(result.registers[1] / 100.0)
data['loadPowerL'] = float(result.registers[2] / 100.0)

result = client.read_input_registers(0x311A,1,unit=1)

data['batterySOC'] = float(result.registers[0] / 100.0)

result = client.read_input_registers(0x311B,1,unit=1)

data['remoteTemp'] = float(result.registers[0] / 100.0)

result = client.read_input_registers(0x3111,1,unit=1)

data['internalTemp'] = float(result.registers[0] / 100.0)

client.close()
 
# Do something with the data
# Write data to mongo

dbclient = MongoClient()

db = dbclient.solarMonitor
solardata = db['solardata']

data['date'] = datetime.datetime.utcnow()

data_id = solardata.insert_one(data).inserted_id

print data_id
print data

dbclient.close()